const Command = require("../structures/Command.js")

class RateLimit extends Command {
  constructor(client) {
    super(client, {
      name: "ratelimit",
      description: "Configures the global cooldown for each trigger.",
      category: "Configuration",
      permLevel: 5,
      usage: "ratelimit <seconds>"
    });
  }

  async run(message, args) {
    if (args.length == 0)
    {
      message.channel.send("Current global rate limit is: " + this.client.config.cooldown)
    }
    else if (args.length == 1)
    {
      this.client.config.cooldown = args[0]
      this.client.saveConfig()
      message.channel.send("Global rate limit changed!")
    }
    else
    {
      message.channel.send("Invalid arguments.")
    }
  }
}

module.exports = RateLimit;